//
//  DataClient.swift
//  20200301-TanmayJain-NYCSchools
//
//  Created by Tanmay Jain on 4/2/20.
//  Copyright © 2020 Tanmay Jain. All rights reserved.
//

import Foundation

// enums can be added for new cases
enum DataClientError: Error {
    case httpError
    case parseError
    case noData
}

// generic data client result for all types of requests/results
enum DataClientResult<T> {
    case success(T)
    case failure(DataClientError)
}

// MARK: DataClient implementation for networking class, shared resource which is guaranteed to be lazily initialized only once, even when accessed across multiple threads simultaneously
final class DataClient {
    
    static let shared = DataClient()

    // MARK: Fetch Method to perform request, made generic for flexibility and re-usability
    func fetch <T: Decodable> (path: Config.Api.Paths,
                               queryItems: [URLQueryItem]? = nil,
                               responseType: T.Type,
                               completion: @escaping (_ returnObject: DataClientResult<T> ) -> Void) {

        var urlComponents = URLComponents(url: Config.Api.absoluteURL(forPath: path), resolvingAgainstBaseURL: false)!

        queryItems?.forEach { item in
            urlComponents.queryItems?.append(item)
        }
        
        // safe check correct Url
        guard let urlForRequest = urlComponents.url else {
            print("ERROR: Incorrect URL")
            return
        }

        let request = URLRequest(url: urlForRequest)
        let task = URLSession(configuration: .default).dataTask(with: request) { (data, response, error) in

            // check for 200
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode != 200 {
                    print("statusCode: \(httpResponse.statusCode)")
                    DispatchQueue.main.async(execute: { () -> Void in
                        completion(.failure(.httpError))
                    })
                    return
                }
            }

            // guard data else failure
            guard let responseData = data else {
                print("Error: did not receive data")
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(.failure(.noData))
                })
                return
            }

            // parse the data
            do {

                let returnResponse = try JSONDecoder().decode(responseType, from: responseData)

                // dispatch completion on main queue
                DispatchQueue.main.async(execute: { () -> Void in
                    completion(.success(returnResponse))
                })
            } catch let error {
                print("Error Parsing JSON: \(error)")
                completion(.failure(.parseError))
                return
            }
        }

        task.resume()
    }
}




