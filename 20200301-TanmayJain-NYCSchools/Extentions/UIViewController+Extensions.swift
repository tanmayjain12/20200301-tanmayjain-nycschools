//
//  UIViewController+Extensions.swift
//  20200301-TanmayJain-NYCSchools
//
//  Created by Tanmay Jain on 4/2/20.
//  Copyright © 2020 Tanmay Jain. All rights reserved.
//

import Foundation
import UIKit

// MARK: extension method to show and hide spinner on all UIViewControllers for better user experience
var vSpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let activitySpinner = UIActivityIndicatorView.init(style: .large)
        activitySpinner.startAnimating()
        activitySpinner.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(activitySpinner)
            onView.addSubview(spinnerView)
        }
        
        vSpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}
