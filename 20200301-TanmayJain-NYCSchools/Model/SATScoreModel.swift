//
//  SATScoreModel.swift
//  20200301-TanmayJain-NYCSchools
//
//  Created by Tanmay Jain on 4/2/20.
//  Copyright © 2020 Tanmay Jain. All rights reserved.
//

import Foundation

struct SATScoreModel: Codable {
    let dbn,schoolName,readingAvgScore, mathAvgScore, writingAvgScore  : String
    
    enum CodingKeys: String, CodingKey {
        case dbn
        case schoolName = "school_name"
        case readingAvgScore = "sat_critical_reading_avg_score"
        case mathAvgScore = "sat_math_avg_score"
        case writingAvgScore = "sat_writing_avg_score"
    }
}
