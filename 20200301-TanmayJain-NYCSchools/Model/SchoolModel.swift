//
//  SchoolModel.swift
//  20200301-TanmayJain-NYCSchools
//
//  Created by Tanmay Jain on 4/1/20.
//  Copyright © 2020 Tanmay Jain. All rights reserved.
//

import Foundation

struct SchoolModel: Codable {

    let dbn, schoolName, location: String?

    enum CodingKeys: String, CodingKey {
        case schoolName = "school_name"
        case location = "primary_address_line_1"
        case dbn
    }
}



