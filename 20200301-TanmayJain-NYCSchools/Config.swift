//
//  Config.swift
//  20200301-TanmayJain-NYCSchools
//
//  Created by Tanmay Jain on 4/2/20.
//  Copyright © 2020 Tanmay Jain. All rights reserved.
//

import Foundation

struct Config {
    struct Api {
        static let baseURL = "https://data.cityofnewyork.us"
        enum Paths: String {
            case schools = "/resource/s3k6-pzi2"
            case satscores = "/resource/f9bf-2cp4"
        }

        static func absoluteURL(forPath path: Paths) -> URL {
            return URL(string: self.baseURL + path.rawValue)!
        }
    }
}
