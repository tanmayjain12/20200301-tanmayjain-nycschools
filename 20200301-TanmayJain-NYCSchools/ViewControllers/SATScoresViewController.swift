//
//  SATScoresViewController.swift
//  20200301-TanmayJain-NYCSchools
//
//  Created by Tanmay Jain on 4/2/20.
//  Copyright © 2020 Tanmay Jain. All rights reserved.
//

import UIKit

// MARK: kept this class to be very simple view with displaying the Avg scores. This class is laoded from the xib intentionally just as an alternative to working with storyboards. Added proper contraints for the labels.
class SATScoresViewController: UIViewController {
    
    @IBOutlet weak var mathScoreLabel: UILabel!
    @IBOutlet weak var readingScoreLabel: UILabel!
    @IBOutlet weak var writingScoreLabel: UILabel!
    var school : SchoolModel?
    var satScoresModel: SATScoreModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = school?.schoolName
        getSATScores()
    }
    
    func SetupScore() {
        mathScoreLabel.text = self.satScoresModel.mathAvgScore
        readingScoreLabel.text = self.satScoresModel.readingAvgScore
        writingScoreLabel.text = self.satScoresModel.writingAvgScore
    }
    
    // data client call to load the SAT score
    func getSATScores() {
        let dbnQuery = URLQueryItem(name: "dbn", value: school?.dbn)
        self.showSpinner(onView: self.view)
        DataClient.shared.fetch(
            path: .satscores,
            queryItems: [dbnQuery],
            responseType: [SATScoreModel].self) { (response) in
                self.removeSpinner()
                switch response {
                case .success(let satScores):
                    guard let satScores = satScores.first else { return }
                    self.satScoresModel = satScores
                    self.SetupScore()
                case .failure(let error):
                    print("Error: \(error)")
                }
        }
    }
}
