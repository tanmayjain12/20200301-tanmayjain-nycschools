//
//  ViewController.swift
//  20200301-TanmayJain-NYCSchools
//
//  Created by Tanmay Jain on 4/1/20.
//  Copyright © 2020 Tanmay Jain. All rights reserved.
//

import UIKit

// MARK: Because the data returned from the API is very large, something that can enhance user experience and usability is using indexed table view so it becomes easy for the user to scroll to a particular school that starts with a particular letter they might be searching for. Another enhancement that can be done is adding UISearchBar on this table view and filtering the list as the user enters the search terms.
class SchoolListViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var schoolList: [SchoolModel]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchSchoolList()
    }
    
    //data client call to fetch the list
    func fetchSchoolList()
    {
        self.showSpinner(onView: self.view)
        DataClient.shared.fetch(
            path: .schools,
            queryItems: [],
            responseType: [SchoolModel].self)
        {(response) in
            self.removeSpinner()
            switch response {
            case .success(let schools):
                self.schoolList = schools
                self.tableView.reloadData()
            case .failure(let error):
                print(error)
                self.presentFailureAlert()
            }
        }
    }
    
    //alert view to cancel loading or retry loading the list on server error
    func presentFailureAlert(){
        let alert = UIAlertController(title: "Oops!", message: "Something went wrong", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Retry?", style: .default, handler: { action in
            self.fetchSchoolList()
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
}

// MARK: TableView dataSource for modularity.
extension SchoolListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return schoolList?.count ?? 0
    }
    
    //School name should always be present, nil coalescing for address field to be safe
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cellReuseIdentifier", for: indexPath)
        cell.textLabel?.text = self.schoolList?[indexPath.item].schoolName
        cell.detailTextLabel?.text = "Location: \(self.schoolList?[indexPath.item].location ?? "Address not available on file")"
        return cell
    }
}

// MARK: TableView delegate for modularity, laoding xib for SATScoresViewController as some larger teams prefer designing separate xibs for seperate views to avoid conflicts on merges. Segues via storyboards can also be used for navigations.
// the data client fetch for SAT scores for selected school can be done in this controller before pushing to the next screen too, message passing can be done through just SATScore object that just populates the next view keeping it simple. If the server returned error for SAT scores API, fail early can be performed and appropriate message can be displayed to the user without navigating to next screen.
extension SchoolListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let satDetailsController = SATScoresViewController()
        satDetailsController.school = self.schoolList?[indexPath.row]
        self.navigationController?.pushViewController(satDetailsController, animated: true)
        self.tableView.deselectRow(at: indexPath, animated: false)
        
    }
}
